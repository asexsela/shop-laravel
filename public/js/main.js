
(function ($) {
    "use strict";

    /*[ Load page ]
    ===========================================================*/
    $(".animsition").animsition({
        inClass: 'fade-in',
        outClass: 'fade-out',
        inDuration: 1500,
        outDuration: 800,
        linkElement: '.animsition-link',
        loading: true,
        loadingParentElement: 'html',
        loadingClass: 'animsition-loading-1',
        loadingInner: '<div class="loader05"></div>',
        timeout: false,
        timeoutCountdown: 5000,
        onLoadEvent: true,
        browser: [ 'animation-duration', '-webkit-animation-duration'],
        overlay : false,
        overlayClass : 'animsition-overlay-slide',
        overlayParentElement : 'html',
        transition: function(url){ window.location.href = url; }
    });
    
    /*[ Back to top ]
    ===========================================================*/
    var windowH = $(window).height()/2;

    $(window).on('scroll',function(){
        if ($(this).scrollTop() > windowH) {
            $("#myBtn").css('display','flex');
        } else {
            $("#myBtn").css('display','none');
        }
    });

    $('#myBtn').on("click", function(){
        $('html, body').animate({scrollTop: 0}, 300);
    });


    /*==================================================================
    [ Fixed Header ]*/
    var headerDesktop = $('.container-menu-desktop');
    var wrapMenu = $('.wrap-menu-desktop');

    if($('.top-bar').length > 0) {
        var posWrapHeader = $('.top-bar').height();
    }
    else {
        var posWrapHeader = 0;
    }
    

    if($(window).scrollTop() > posWrapHeader) {
        $(headerDesktop).addClass('fix-menu-desktop');
        $(wrapMenu).css('top',0); 
    }  
    else {
        $(headerDesktop).removeClass('fix-menu-desktop');
        $(wrapMenu).css('top',posWrapHeader - $(this).scrollTop()); 
    }

    $(window).on('scroll',function(){
        if($(this).scrollTop() > posWrapHeader) {
            $(headerDesktop).addClass('fix-menu-desktop');
            $(wrapMenu).css('top',0); 
        }  
        else {
            $(headerDesktop).removeClass('fix-menu-desktop');
            $(wrapMenu).css('top',posWrapHeader - $(this).scrollTop()); 
        } 
    });


    /*==================================================================
    [ Menu mobile ]*/
    $('.btn-show-menu-mobile').on('click', function(){
        $(this).toggleClass('is-active');
        $('.menu-mobile').slideToggle();
    });

    var arrowMainMenu = $('.arrow-main-menu-m');

    for(var i=0; i<arrowMainMenu.length; i++){
        $(arrowMainMenu[i]).on('click', function(){
            $(this).parent().find('.sub-menu-m').slideToggle();
            $(this).toggleClass('turn-arrow-main-menu-m');
        })
    }

    $(window).resize(function(){
        if($(window).width() >= 992){
            if($('.menu-mobile').css('display') == 'block') {
                $('.menu-mobile').css('display','none');
                $('.btn-show-menu-mobile').toggleClass('is-active');
            }

            $('.sub-menu-m').each(function(){
                if($(this).css('display') == 'block') { console.log('hello');
                    $(this).css('display','none');
                    $(arrowMainMenu).removeClass('turn-arrow-main-menu-m');
                }
            });
                
        }
    });


    /*==================================================================
    [ Show / hide modal search ]*/
    $('.js-show-modal-search').on('click', function(){
        $('.modal-search-header').addClass('show-modal-search');
        $(this).css('opacity','0');
    });

    $('.js-hide-modal-search').on('click', function(){
        $('.modal-search-header').removeClass('show-modal-search');
        $('.js-show-modal-search').css('opacity','1');
    });

    $('.container-search-header').on('click', function(e){
        e.stopPropagation();
    });


    /*==================================================================
    [ Isotope ]*/
    var $topeContainer = $('.isotope-grid');
    var $filter = $('.filter-tope-group');

    // filter items on button click
    $filter.each(function () {
        $filter.on('click', 'button', function () {
            var filterValue = $(this).attr('data-filter');
            $topeContainer.isotope({filter: filterValue});
        });
        
    });

    // init Isotope
    $(window).on('load', function () {
        var $grid = $topeContainer.each(function () {
            $(this).isotope({
                itemSelector: '.isotope-item',
                layoutMode: 'fitRows',
                percentPosition: true,
                animationEngine : 'best-available',
                masonry: {
                    columnWidth: '.isotope-item'
                }
            });
        });
    });

    var isotopeButton = $('.filter-tope-group button');

    $(isotopeButton).each(function(){
        $(this).on('click', function(){
            for(var i=0; i<isotopeButton.length; i++) {
                $(isotopeButton[i]).removeClass('how-active1');
            }

            $(this).addClass('how-active1');
        });
    });

    /*==================================================================
    [ Filter / Search product ]*/
    $('.js-show-filter').on('click',function(){
        $(this).toggleClass('show-filter');
        $('.panel-filter').slideToggle(400);

        if($('.js-show-search').hasClass('show-search')) {
            $('.js-show-search').removeClass('show-search');
            $('.panel-search').slideUp(400);
        }    
    });

    $('.js-show-search').on('click',function(){
        $(this).toggleClass('show-search');
        $('.panel-search').slideToggle(400);

        if($('.js-show-filter').hasClass('show-filter')) {
            $('.js-show-filter').removeClass('show-filter');
            $('.panel-filter').slideUp(400);
        }    
    });




    /*==================================================================
    [ Cart ]*/
    $('.js-show-cart').on('click',function(){
        $('.js-panel-cart').addClass('show-header-cart');
    });

    $('.js-hide-cart').on('click',function(){
        $('.js-panel-cart').removeClass('show-header-cart');
    });

    /*==================================================================
    [ Cart ]*/
    $('.js-show-sidebar').on('click',function(){
        $('.js-sidebar').addClass('show-sidebar');
    });

    $('.js-hide-sidebar').on('click',function(){
        $('.js-sidebar').removeClass('show-sidebar');
    });


    /*==================================================================
    [ Rating ]*/
    $('.wrap-rating').each(function(){
        var item = $(this).find('.item-rating');
        var rated = -1;
        var input = $(this).find('input');
        $(input).val(0);

        $(item).on('mouseenter', function(){
            var index = item.index(this);
            var i = 0;
            for(i=0; i<=index; i++) {
                $(item[i]).removeClass('zmdi-star-outline');
                $(item[i]).addClass('zmdi-star');
            }

            for(var j=i; j<item.length; j++) {
                $(item[j]).addClass('zmdi-star-outline');
                $(item[j]).removeClass('zmdi-star');
            }
        });

        $(item).on('click', function(){
            var index = item.index(this);
            rated = index;
            $(input).val(index+1);
        });

        $(this).on('mouseleave', function(){
            var i = 0;
            for(i=0; i<=rated; i++) {
                $(item[i]).removeClass('zmdi-star-outline');
                $(item[i]).addClass('zmdi-star');
            }

            for(var j=i; j<item.length; j++) {
                $(item[j]).addClass('zmdi-star-outline');
                $(item[j]).removeClass('zmdi-star');
            }
        });
    });
    
    // [ Show modal1 ]

	$(document).on('click','.js-show-modal1',function(e){
		let token = $('meta[name="csrf-token"]').attr('content');
		var sl = $(this).attr('data-product');
		e.preventDefault();
		$.ajax({
	
			type:'POST',
			url:"/product_details/" + sl,
			data: {
				"_method": 'POST',
				"_token": token,
			},
			success:function(data){
                let name = data.name,
                img = data.img,
                price = data.price,
                desc = data.desc,
                id = data.id,
                color = data.color,
                size = data.size,
                zero = 0;

                console.log(data);
                // console.log('Размер ' + size);

                if (color != null ) {

                    for (let i = 0; i < color.length; i++) {
                        $('.default_color').append('<option>' + color[i] + '</option>');
                    }
                    
                } 

                if (size != null) {

                    for (let i = 0; i < size.length; i++) {
                        $('.default_size').append('<option>' + size[i] + '</option>');
                    }

                }

				$('.modal_name').text(name);
				$('.modal_price').text(price + ' руб.');
				$('.modal_desc').text(desc);
				$('.modal_src_img').attr('src', img);
				$('.modal_href_img').attr('href', img);

				$('input[name="id"]').attr('value', id);
				$('input[name="name"]').attr('value', name);
				$('.js-modal1').addClass('show-modal1');
			},
			error:function(){
                console.log('Error modal');
			},
		});
    });

    $('.js-hide-modal1').on('click',function(){
        $('.js-modal1').removeClass('show-modal1');
        $('.modal_name').text('');
        $('.modal_price').text('');
        $('.modal_desc').text('');
        $('.modal_src_img').attr('');
        $('.modal_href_img').attr('');
        $('.add-cart').show();
        $('.run-to-cart').hide();
        $('.num-product').val(1);
        $('.default_size').html('<option value="no">Выберете размер</option>');
        $('.default_color').html('<option value="no">Выберете цвет</option>');
        
    });

// add to cart
	$(document).on('click','.js-addcart-detail',function(e){
		let token = $('meta[name="csrf-token"]').attr('content');
		var formData = $('#add_to_cart').serializeArray();
        var size = formData[0]['value'];
        var color = formData[1]['value'];
        var id = formData[2]['value'];
        var name = formData[3]['value'];
        var amount = parseInt(formData[4]['value'], 10);
        var count_cart = parseInt($('.header_cart').attr('data-notify'), 10);
        console.log(formData);

		e.preventDefault();
		$.ajax({
	
			type:'POST',
			url:"/addtocart/" + id + '/' + amount + '/' + size + '/' + color ,
			data: {
				"_method": 'POST',
				"_token": token,
			},
			success:function(data){
                // console.log( amount);
                if (data === 'success') {
                    swal(name, "добавлен в корзину !", "success");
                    $('.header_cart').attr('data-notify', count_cart + amount);
                    $(e.target).hide();
                    $(e.target).next('a').show();
                }
			},
			error:function(error){
                console.log(error);
			},
		});
    });


    $('.search').keyup(function(e) {
        let token = $('meta[name="csrf-token"]').attr('content');

        $.ajax({
            type: 'POST',
            url: '/search/' + this.value,
            data: {
                '_method': 'POST',
                '_token': token
            },
            success: function(data) {
                // console.log(data);
                
                $('.box-catalog').css('height', 'auto');
                $('.box-catalog').html(data);
            }
        });

    });
    
    // Save For Letter

    $('.js-addwish-b2').click(function(e) {

        e.preventDefault();

        let token = $('meta[name="csrf-token"]').attr('content');
        let id = $(this).attr('data-id');

        $.ajax({
            type: 'POST',
            url: '/letter/' + id,
            data: {
                '_method': 'POST',
                '_token': token
            },
            success: function(data) {
                console.log(data);

                let num = parseInt($('.letter').attr('data-notify'), 10);
                if (data.status === 1) {

                    swal(data.name, "добавлен в список желаний!", "success");
                    $(e.target).parent().addClass('js-addedwish-b2');
                    $('.letter').attr('data-notify', num + 1);

                } else if (data.status === 3) {
                    console.log(data);
                    $('.letter').attr('data-notify', num - 1);
                    swal(data.name, "удален из списка желаний !", "success");
                    $(e.target).parent().removeClass('js-addedwish-b2');
                } 
                
            },
            error: function(error) {
                console.log(error);
                swal('Извините', " чтобы сохранить товар вы должны войти или зарегистрироваться", "info");
            }
        });

    });

    

})(jQuery);

