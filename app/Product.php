<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{

    public function category() {

        return $this->belongsToMany(Category::class);

    }

    public function user() {

        return $this->belongsToMany(User::class, 'user_product');

    }

    public function presentPrice() {

        if (PHP_OS == 'WINNT') {
            
            return sprintf('%01.2f', $this->price);

        } else {
            
            return money_format('%i', $this->price);

        }
    }

}
