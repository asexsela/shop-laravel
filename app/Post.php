<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
   

    public function post_date() {
        $month = date('n') - 1; 
        $arr = ['янв','фев','мар','апр','май','июн','июл','авг','сен','окт','ноя','дек'];
        return $this->created_at->format($arr[$month] . ' Y');
    }

    public function catPost() {

        return $this->belongToMany(PostCategory::class, 'id');
    }
}
