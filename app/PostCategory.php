<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostCategory extends Model
{
    protected $table = 'post_categories';


    public function post() {

        return $this->hasMany(Post::class, 'category_id');

    }
}
