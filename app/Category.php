<?php

namespace App;

use Illuminate\Support\Carbon;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    
    public function product() {

        return $this->hasMany(Product::class);

    }

}
