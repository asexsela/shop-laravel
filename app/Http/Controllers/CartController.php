<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Support\Facades\Validator;
use App\Product;

class CartController extends Controller
{
    public function index() {

        $tax = config('cart.tax') / 100;
        $discount = session()->get('coupon')['discount'] ?? 0;
        $newSubtotal = (Cart::subtotal() - $discount) ?? Cart::subtotal();
        $newTax = $newSubtotal * $tax;
        $newTotal = $newSubtotal + $newTax;

        return view('layouts.shoping_cart')->with([
            'discount' => $discount,
            'newTax' => $newTax,
            'newTotal' => $newTotal,
            'newSubtotal' => $newSubtotal
        ]);
    }

    // public function saveForLater() {

    //     return view('layouts.saveForLater');

    // }

    // public static function store(Request $request) {

    //     $dublicate = Cart::search(function ($cartItem, $rowId) use ($request) {
    //         return $cartItem->id === $request->id;
    //     });

    //     if ($dublicate->isNotEmpty()) {

    //         return redirect()->route('cart.index')->with('success_message', 'Товар уже добавлен в корзину');            

    //     }

    //     Cart::add($request->id, $request->name, 1, $request->price, [
    //         'img' => $request->img,
    //     ])->associate('App\Product');

        
    //     // return back();
    //     return redirect()->route('cart.index')->with('success_message', 'Товар добавлен в корзину');
    // }

    public function ajaxStore($id, $amount, $size, $color, Request $request) {

        

        $product = Product::where('id', $id)->firstOrFail();

        $products_id = $product->id;
        $products_name = $product->name;
        $products_price = $product->price;
        $products_img = $product->img;
        $products_size = $size;
        $products_color = $color;
        

        // $dublicate = Cart::search(function ($cartItem, $rowId) use ($id) {
        //     return $cartItem->id === $id;
        // });

        // if ($dublicate->isNotEmpty()) {

        //     return 'dublicate';            

        // }

        Cart::add($products_id, $products_name , $amount , $products_price, 
        [
            'img' => $products_img,
            'size' => $products_size,
            'color' => $products_color
        ])->associate('App\Product');
       
        return 'success';
        // return $request;

    }

    public function empty() {

        Cart::destroy();

        return back()->with('success_message', 'Корзина очищена');

    }

    public function destroy($id) {

        Cart::remove($id);

        return back()->with('success_message', 'Товар удален из корзины');

    }

    // public function switchToSaveForLater($id) {

    //    $item = Cart::add($id);

    // //    Cart::remove($id);


    //    Cart::instance('saveForLater')->add($item->id, $item->name, 1, $item->price, [
    //     'img' => $item->img,
    // ])->associate('App\Product');
    
    // // return back();
    // return redirect()->route('cart.saveForLater')->with('success_message', 'Товар добавлен в список желаний');
    // }


    public function update(Request $request, $id) {

        $validator = Validator::make($request->all(), [
            'quantity' => 'required|numeric'
        ]);

        if($validator->fails()) {
            session()->flash('errors', collect(['Максимальное количество товара 5']));

             return response()->json(['success' => false], 400);
        }

        Cart::update($id, $request->quantity);

        session()->flash('success_message', 'Количество товаров обновлено');

        return response()->json(['success' => true]);
        
        
    }
}
