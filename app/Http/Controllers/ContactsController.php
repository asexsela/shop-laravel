<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;

class ContactsController extends Controller
{
    public function index(Request $request) {

        $categories = Category::all();
        
        return view('layouts.contacts', [
            'categories' => $categories,
        ]);
    }
}
