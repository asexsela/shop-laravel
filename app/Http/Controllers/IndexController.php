<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Product;
class IndexController extends Controller
{
    public function index() {

        $banner = Category::all()->where('is_banner','==', 1);
        $category = Category::all();
        $products = Product::inRandomOrder()->take(8)->get();

        return view('layouts.home', [
            'banner' => $banner,
            'products' => $products,
            'category' => $category,
        ]);
    }
}
