<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;

class AboutController extends Controller
{
    public function index() {

        $categories = Category::all();

        return view('layouts.about', [
            'categories' => $categories,
        ]);
    }
}
