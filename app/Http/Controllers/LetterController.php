<?php

namespace App\Http\Controllers;

use App\User;
use App\Letter;
use App\Product;
use App\UserProduct;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LetterController extends Controller
{
    
    public function saveLetter($id) {

        $user_id = auth()->user()->id;
        
        
        $dublicate = UserProduct::where('user_id', $user_id)->where('product_id', $id)->first();
        
        // return $dublicate;

        $product = Product::where('id', $id)->firstOrFail();
        
        
        if (!empty($dublicate)) {
            
            // $product->letter = null;
            // $product->save();

            $dublicate->delete();
            
            return [
                'status' => 3,
                'name' => $product->name,
            ];
        }
        
        $letter = new UserProduct;
        $letter->product_id = $product->id;
        $letter->user_id = $user_id;
        $letter->save();

        // $product->letter = $user_id;
        // $product->save();

        if (Auth::check()) {

            return [
                'status' => 1,
                'name'  => $product->name,
            ];

        } 

    }



}

