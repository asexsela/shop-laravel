<?php

namespace App\Http\Controllers;

use App\Coupon;
use Illuminate\Http\Request;
use Gloudemans\Shoppingcart\Facades\Cart;

class CouponsController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $coupon = Coupon::where('code', $request->coupon)->first();

        if (!$coupon) {

            return redirect()->route('cart.index')->withErrors('Неверный код. Пожалуйста попробуйте снова.');

        }

        session()->put('coupon', [
            'name' => $coupon->code,
            'discount' => $coupon->discount(Cart::subtotal()),
            
        ]);

        return redirect()->route('cart.index')->with('success_message', 'Купон успешно применен');
        
        // dd($coupon);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {

        session()->forget('coupon');
        
        return redirect()->route('cart.index')->with('success_message', 'Купон успешно удален');

    }
}
