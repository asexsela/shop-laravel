<?php

namespace App\Http\Controllers;

use App\Post;
use App\PostCategory;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class BlogController extends Controller
{
    

    public function index() {

        $posts = Post::paginate(2);
        
        return view('layouts.blog')->with([
            'posts' => $posts
            ]);
    }


    public function indexDetails($alias) {

        $simple = Post::where('slug', $alias)->firstOrFail();


        return view('layouts.blog_details')->with('simple', $simple);
    }

    public function getCategory($alias) {

        $id = PostCategory::where('slug', $alias)->firstOrFail();
        $category = PostCategory::find($id->id)->post()->paginate(2);

        return view('layouts.blog')->with('posts', $category);

    }
}

