<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Product;

class CatalogController extends Controller
{
    public function index() {
        
        $category = Category::all();
        $product = Product::paginate(8);

        if (request()->sort == 'low_high') {

            $product = Product::orderBy('price')->paginate(8);

        } elseif (request()->sort == 'high_low') {

            $product = Product::orderBy('price', 'desc')->paginate(8);

        }
        
        return view('layouts.catalog', [

            'category' => $category,
            'products' => $product,

        ]);
    }

    public function category($slug) {

        $cat_id = Category::where('slug', $slug)->firstOrFail();
        $category = Category::all();
        $products = Category::find($cat_id->id)->product()->paginate(8);
        return view('layouts.catalog')->with([
            'category' => $category,
            'products' => $products,
            'cat_id'   => $cat_id
        ]);


    }

    public function indexProduct($product) {

        $relateds = Product::inRandomOrder()->take(8)->get();
        $product = Product::where('slug', $product)->firstOrFail();
        $size = $product->size;
        $colors = $product->color;


        return view('layouts.product', [

            'relateds' => $relateds,
            'products' => $product,
            'size'     => json_decode($size),
            'colors'     => json_decode($colors)

            ]);
    }

    public function product_details($slug) {

        $product = Product::where('slug', $slug)->firstOrFail();

        $result = [

            'id'    => $product->id,
            'name'  => $product->name,
            'img'   => '/' . $product->img,
            'price' => presentPrice($product->price),
            'desc'  => $product->description,
            'slug'  => $product->slug,
            'size'     => json_decode($product->size),
            'color'     => json_decode($product->color)

        ];
        
        return $result;
    }

    public function search($s) {

        $products = Product::where('name', 'like', $s . '%')->get();

        return view('components.search_product', ['products' => $products]);

    }


}
