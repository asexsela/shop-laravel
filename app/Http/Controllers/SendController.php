<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use Illuminate\Mail\Mailable;
use App\Mail\MailClass;
use Illuminate\Support\Facades\Mail;

class SendController extends Controller
{
    public function sendContacts(Request $request) {

        $email = $request->email;
        $msg = $request->msg;

        Mail::to("asexsela@gmail.com")->send(new MailClass($email, $msg));
        
        if(count(Mail::failures()) > 0){
            return 0;
        } else {
            return 1;
        }
    }
}