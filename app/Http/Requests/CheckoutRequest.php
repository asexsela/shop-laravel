<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CheckoutRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email_checkout' => 'required|email',
            'name_cart_checkout' => 'required',
            'addr_one_checkout' => 'required',
            'city_checkout' => 'required',
            'reg_checkout' => 'required',
            'postcode_checkout' => 'required|numeric',
            'phone_checkout' => 'required|numeric',
        ];
    }
}
