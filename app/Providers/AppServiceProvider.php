<?php

namespace App\Providers;


use App\Post;
use App\Product;
use App\Category;
use App\PostCategory;
use Jenssegers\Date\Date;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        Date::setlocale(config('app.locale'));
        
        
        view()->share('categoryy', Category::all());
        
        view()->share('postCategories', PostCategory::all());
        
        view()->share('featuredProducts', Product::inRandomOrder()->take(3)->get());
        
        view()->share('arhives', Post::all());




    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
