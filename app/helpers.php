<?php

use App\UserProduct;


function presentPrice($price) {

    if (PHP_OS == 'WINNT') {
            
        return sprintf('%01.2f', $price);

    } else {
        
        return money_format('%i', $price);

    }

}

function checkLetter($user_id, $product_id) {

    $check = UserProduct::where('user_id', $user_id)->where('product_id', $product_id)->first();

    if (!empty($check)) {

        return true;

    } else {

        return false;

    }
}
