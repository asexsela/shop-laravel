<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('img');
            $table->string('slug')->unique();
            $table->integer('amount')->default(0);
            $table->integer('is_new')->default(0);
            $table->string('price');
            $table->text('description');
            $table->text('info');
            $table->text('body');
            $table->enum('size', ['NO','S','M','L','XL']);
            $table->enum('color', ['NO','GREY','BLUE','WHITE','RED']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
