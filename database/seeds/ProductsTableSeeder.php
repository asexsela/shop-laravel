<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i < 17; $i++) {

            DB::table('products')->insert([
                'name' => 'Продукт ' . $i,
                'img' => '/images/product-0'. $i .'.jpg',
                'category_id' => null,
                'slug' => 'product' . $i,
                'amount' => 1,
                'is_new' => 0,
                'price' => '1000',
                'description' => 'Небольшое описание',
                'info' => 'Характеристики товара',
                'body' => 'Большое описание',
                'size' => 'S',
                'color' => 'Красный',
            ]);
        }

    }
}
