<?php
use App\CatProduct;

use Illuminate\Database\Seeder;

class CatProductSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CatProduct::create([
            'categody_id' => '1',
            'product_id' => '2',
        ]);
    }
}
