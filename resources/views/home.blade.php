@extends('index', ['title' => 'Личный кабинет'])



@section('content')
<section class="bg0 p-t-100 p-b-140">
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Личные данные</div>

                <div class="card-body">
                    Имя:
                    {{ auth()->user()->name }}
                    <br>
                    Email:
                    {{ auth()->user()->email }}
                    <br>
                    Дата регистрации: 
                    {{ Date::parse(auth()->user()->created_at)->format('d F Y') }}
                    <br>
                    Аватар:
                    {{-- <img src="{{ asset('/storage/' . auth()->user()->avatar) }}" alt=""> --}}

                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
</section>
<section class="bg0 p-t-23 p-b-140">
    <div class="container">
        <div class="p-b-10">
            <h3 class="ltext-103 cl5">
               Избранные товары
            </h3>
        </div>
        <div class="row isotope-grid">

                @foreach ($saveLetter as $product)
                    <div class="col-sm-6 col-md-4 col-lg-3 p-b-35 isotope-item {{ $product->category }}">
                        <!-- Block2 -->
                        <div class="block2">
                            <div class="block2-pic hov-img0">
                            <img src="{{ asset($product->img) }}" alt="IMG-PRODUCT">
                            <form action="/product_details" method="POST">
                        
                                {{ csrf_field() }}
            
                                <button data-product="" type="submit" class="block2-btn flex-c-m stext-103 cl2 size-102 bg0 bor2 hov-btn1 p-lr-15 trans-04 js-show-modal1">Подробнее</button>
            
                            </form>
                            </div>

                            <div class="block2-txt flex-w flex-t p-t-14">
                                <div class="block2-txt-child1 flex-col-l ">
                                <a  href="/shop/product/" class="stext-104 cl4 hov-cl1 trans-04 js-name-b2 p-b-6">
                                    {{ $product->name }}
                                    </a>

                                    <span class="stext-105 cl3">
                                        {{ presentPrice($product->price) }} руб.
                                    </span>
                                </div>

                                <div class="block2-txt-child2 flex-r p-t-3">
                                <a data-id="{{ $product->id }}" href="#" class="btn-addwish-b2 dis-block pos-relative js-addwish-b2 {{ (auth()->check() && checkLetter(auth()->user()->id, $product->id)) ? 'js-addedwish-b2' : '' }}">
                                        <img class="icon-heart1 dis-block trans-04" src="{{ asset('images/icons/icon-heart-01.png') }}" alt="ICON">
                                        <img class="icon-heart2 dis-block trans-04 ab-t-l" src="{{ asset('images/icons/icon-heart-02.png') }}" alt="ICON">
                                    </a>
                                </div>

                            </div>
                        </div>
                    </div>
                @endforeach
            
        </div>

            

        <!-- Load more -->
        <div class="flex-c-m flex-w w-full p-t-45">
        <a href="{{ route('shop') }}" class="flex-c-m stext-101 cl5 size-103 bg2 bor1 hov-btn1 p-lr-15 trans-04">
                Больше товаров
            </a>
        </div>
    </div>
</section>
@endsection
