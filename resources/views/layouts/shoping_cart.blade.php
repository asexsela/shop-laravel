@extends('index', ['title' => 'Корзина'])


@section('content')

    @include('components.breadcrumbs')

    @include('components.cart')
    
@endsection

@section('js')
<script src="{{ asset('js/app.js') }}"></script>
    <script>
    
        $(function() {

            const classname = document.querySelector('.num-product');
            

            $('.btn-num-product-down').on('click', function(){

                var numProduct = Number($(this).next().val());

                if(numProduct > 0) $(classname).val(numProduct - 1);

                const id = $(this).next().attr('data-id');
                
                axios.patch(`/cart/${id}`, {

                    quantity: classname.value,

                })
                .then(function (response) {
                    window.location.href = '{{ route('cart.index') }}';
                })
                .catch(function (error) {
                    console.log(error);
                    window.location.href = '{{ route('cart.index') }}';

                });
            });

            $('.btn-num-product-up').on('click', function(){

                var numProduct = Number($(this).prev().val());

                $(classname).val(numProduct + 1);   

                const id = $(this).prev().attr('data-id');

                axios.patch(`/cart/${id}`, {

                    quantity: classname.value,

                })
                .then(function (response) {
                    window.location.href = '{{ route('cart.index') }}';
                })
                .catch(function (error) {
                    console.log(error);
                    window.location.href = '{{ route('cart.index') }}';

                });
            });

            
            
        });
    
    </script>
@endsection