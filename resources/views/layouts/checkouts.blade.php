@extends('index', ['title' => 'Оплата'])

@section('content')

    @include('components.checkouts')

@endsection

@section('js')
<script>
        
        // Create a Stripe client.
            var stripe = Stripe('pk_test_HC1ik7riBOTO4lxX5xV9HfZH');

            // Create an instance of Elements.
            var elements = stripe.elements();

            // Custom styling can be passed to options when creating an Element.
            // (Note that this demo uses a wider set of styles than the guide below.)
            var style = {
            base: {
                color: '#32325d',
                fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
                fontSmoothing: 'antialiased',
                fontSize: '16px',
                '::placeholder': {
                color: '#aab7c4'
                }
            },
            invalid: {
                color: '#fa755a',
                iconColor: '#fa755a'
            }
            };

            // Create an instance of the card Element.
            var card = elements.create('card', {
                    style: style,
                    hidePostalCode: true,
                });

            // Add an instance of the card Element into the `card-element` <div>.
            card.mount('#card-element');

            // Handle real-time validation errors from the card Element.
            card.addEventListener('change', function(event) {
            var displayError = document.getElementById('card-errors');
            if (event.error) {
                displayError.textContent = event.error.message;
            } else {
                displayError.textContent = '';
            }
            });

            // Handle form submission.
            var form = document.getElementById('payment-form');

            form.addEventListener('submit', function(event) {

            event.preventDefault();
            
            document.getElementById('complete-order').disabled = true;

            var options = {
                name: document.getElementById('name_cart_checkout').value,
                address_line1: document.getElementById('addr_one_checkout').value,
                address_city: document.getElementById('city_checkout').value,
                address_state: document.getElementById('reg_checkout').value,
                address_zip: document.getElementById('postcode_checkout').value

            }
            // console.log(options);


            stripe.createToken(card, options).then(function(result) {
            console.log(options);

                if (result.error) {
                // Inform the user if there was an error.
                var errorElement = document.getElementById('card-errors');

                errorElement.textContent = result.error.message;

                document.getElementById('complete-order').disabled = false;

                } else {
                // Send the token to your server.
                stripeTokenHandler(result.token);
                }
            });
            });

            // Submit the form with the token ID.
            function stripeTokenHandler(token) {
            // Insert the token ID into the form so it gets submitted to the server
            var form = document.getElementById('payment-form');
            var hiddenInput = document.createElement('input');
            hiddenInput.setAttribute('type', 'hidden');
            hiddenInput.setAttribute('name', 'stripeToken');
            hiddenInput.setAttribute('value', token.id);
            form.appendChild(hiddenInput);

            // Submit the form
            form.submit();
            }

  
</script>
@endsection