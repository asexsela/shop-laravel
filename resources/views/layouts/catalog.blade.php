

@extends('index', ['title' => 'Каталог'])


@section('content')


	<div class="bg0 m-t-23 p-b-140">
		<div class="container">

            @include('components.filter')

			<div class="row isotope-grid box-catalog">
                
                @include('components.products_catalog')

			</div>

            {{ $products->links() }}

		
		</div>
	</div>
		

    
@endsection