@extends('index', ['title' =>  $products->name ])
@section('content')

    @include('components.breadcrumbs')

    @include('components.product_detail')

@endsection
