@extends('index', ['title' => 'Главная'])

@section('content')

	<!-- Slider -->
	@include('components.slider')

	<!-- Banner -->
	@include('components.banner')

	<!-- Product -->
    @include('components.products_index')

    
@endsection
