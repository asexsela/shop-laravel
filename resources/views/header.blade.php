		<!-- Header desktop -->
		<div class="container-menu-desktop">
			<!-- Topbar -->
			<div class="top-bar">
				<div class="content-topbar flex-sb-m h-full container">
					<div class="left-top-bar">
							Бесплатная доставка от 1000 руб.
					</div>

					<div class="right-top-bar flex-w h-full">
						{{-- <a href="{{ route('faq') }}" class="flex-c-m trans-04 p-lr-25">
							Поддержка
						</a> --}}

                        @guest
                            <a href="{{ route('login') }}" class="flex-c-m trans-04 p-lr-25">
                                Вход
                            </a>
                            <a href="{{ route('register') }}" class="flex-c-m trans-04 p-lr-25">
                                Регистрация
                            </a>
                        @else   
                            <a href="{{ route('home') }}" class="flex-c-m trans-04 p-lr-25">
                                Личный кабинет
                            </a>
                            <a href="{{ route('logout') }}" class="flex-c-m trans-04 p-lr-25" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                Выход
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        @endguest
					</div>
				</div>
			</div>

			<div class="wrap-menu-desktop how-shadow1">
				<nav class="limiter-menu-desktop container">
					
					<!-- Logo desktop -->		
                <a href="{{ route('index') }}" class="logo">
                    <img src="{{ asset('images/icons/logo-01.png') }}" alt="IMG-LOGO">
					</a>

					<!-- Menu desktop -->
					<div class="menu-desktop">
						<ul class="main-menu">
							{{ menu('header', 'header_menu') }}
						</ul>
					</div>	

					<!-- Icon header -->
					<div class="wrap-icon-header flex-w flex-r-m">
						<div class="icon-header-item cl2 hov-cl1 trans-04 p-l-22 p-r-11 js-show-modal-search">
							<i class="zmdi zmdi-search"></i>
						</div>

					<div class="icon-header-item cl2 hov-cl1 trans-04 p-l-22 p-r-11 icon-header-noti header_cart js-show-cart" data-notify="{{ Cart::instance('default')->count() }}">
							<i class="zmdi zmdi-shopping-cart"></i>
						</div>

                        <a href="{{ route('home') }}" class="icon-header-item cl2 hov-cl1 trans-04 p-l-22 p-r-11 icon-header-noti letter" data-notify="{{ (Auth::check()) ?  $saveLetter->count() : 0 }}">
							<i class="zmdi zmdi-favorite-outline"></i>
						</a>
					</div>
				</nav>
			</div>	
		</div>

		<!-- Header Mobile -->
		<div class="wrap-header-mobile">
			<!-- Logo moblie -->		
			<div class="logo-mobile">
            	<a href="{{ route('index') }}"><img src="{{ asset('images/icons/logo-01.png') }}" alt="IMG-LOGO"></a>
			</div>

			<!-- Icon header -->
			<div class="wrap-icon-header flex-w flex-r-m m-r-15">
				<div class="icon-header-item cl2 hov-cl1 trans-04 p-r-11 js-show-modal-search">
					<i class="zmdi zmdi-search"></i>
				</div>

				<div class="icon-header-item cl2 hov-cl1 trans-04 p-r-11 p-l-10 icon-header-noti js-show-cart" data-notify="{{ Cart::instance('default')->count() }}">
					<i class="zmdi zmdi-shopping-cart"></i>
				</div>

				<a href="#" class="dis-block icon-header-item cl2 hov-cl1 trans-04 p-r-11 p-l-10 icon-header-noti" data-notify="0">
					<i class="zmdi zmdi-favorite-outline"></i>
				</a>
			</div>

			<!-- Button show menu -->
			<div class="btn-show-menu-mobile hamburger hamburger--squeeze">
				<span class="hamburger-box">
					<span class="hamburger-inner"></span>
				</span>
			</div>
		</div>


		<!-- Menu Mobile -->
		<div class="menu-mobile">
			<ul class="topbar-mobile">
				<li>
					<div class="left-top-bar">
						Бесплатная доставка от 1000 руб.
					</div>
				</li>

				<li>
					<div class="right-top-bar flex-w h-full">
						<a href="{{ route('faq') }}" class="flex-c-m p-lr-10 trans-04">
							Поддержка
						</a>

						@guest
                            <a href="{{ route('login') }}" class="flex-c-m p-lr-10 trans-04">
                                Вход
                            </a>
                            <a href="{{ route('register') }}" class="flex-c-m p-lr-10 trans-04">
                                Регистрация
                            </a>
                        @else   
                            <a href="{{ route('home') }}" class="flex-c-m p-lr-10 trans-04">
                                Личный кабинет
                            </a>
                            <a href="{{ route('logout') }}" class="flex-c-m p-lr-10 trans-04" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                Выход
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        @endguest
					</div>
				</li>
			</ul>

			<ul class="main-menu-m">
				{{ menu('header', 'header_menu') }}
			</ul>
		</div>

		<!-- Modal Search -->
		<div class="modal-search-header flex-c-m trans-04 js-hide-modal-search">
			<div class="container-search-header">
				<button class="flex-c-m btn-hide-modal-search trans-04 js-hide-modal-search">
                <img src="{{ asset('images/icons/icon-close2.png') }}" alt="CLOSE">
				</button>

				<form class="wrap-search-header flex-w p-l-15">
					<button class="flex-c-m trans-04">
						<i class="zmdi zmdi-search"></i>
					</button>
					<input class="plh3" type="text" name="search" placeholder="Поиск...">
				</form>
			</div>
		</div>