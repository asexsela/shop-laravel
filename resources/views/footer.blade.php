<!-- Footer -->
<footer class="bg3 p-t-75 p-b-32">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-lg-3 p-b-50">
                <h4 class="stext-301 cl0 p-b-30">
                    Категории
                </h4>

                <ul>
                    @foreach ($categoryy as $category)
                        <li class="p-b-10">
                        <a href="/shop/{{ $category->slug }}" class="stext-107 cl7 hov-cl1 trans-04">
                                {{ $category->name }}
                            </a>
                        </li>
                    @endforeach

                </ul>
            </div>

            <div class="col-sm-6 col-lg-3 p-b-50">
                <h4 class="stext-301 cl0 p-b-30">
                    Помощь
                </h4>

                <ul>
                    <li class="p-b-10">
                        <a href="{{ route('track_order') }}" class="stext-107 cl7 hov-cl1 trans-04">
                            Отследить заказ
                        </a>
                    </li>

                    <li class="p-b-10">
                        <a href="{{ route('order_return') }}" class="stext-107 cl7 hov-cl1 trans-04">
                            Возвращение заказа 
                        </a>
                    </li>

                    <li class="p-b-10">
                        <a href="{{ route('shiping') }}" class="stext-107 cl7 hov-cl1 trans-04">
                            Доставка
                        </a>
                    </li>

                    <li class="p-b-10">
                        <a href="{{ route('faq') }}" class="stext-107 cl7 hov-cl1 trans-04">
                            Вопросы/ответы
                        </a>
                    </li>
                </ul>
            </div>

            <div class="col-sm-6 col-lg-3 p-b-50">
                <h4 class="stext-301 cl0 p-b-30">
                    КАК С НАМИ СВЯЗАТЬСЯ
                </h4>

                <p class="stext-107 cl7 size-201">
                    У вас есть вопросы ? Вы можете подойти к нам в офис по адресу: ..., позвонив по телефону: ... или оставить заявку на сайте.
                </p>

                <div class="p-t-27">
                    <a href="#" class="fs-18 cl7 hov-cl1 trans-04 m-r-16">
                        <i class="fa fa-facebook"></i>
                    </a>

                    <a href="#" class="fs-18 cl7 hov-cl1 trans-04 m-r-16">
                        <i class="fa fa-instagram"></i>
                    </a>

                    <a href="#" class="fs-18 cl7 hov-cl1 trans-04 m-r-16">
                        <i class="fa fa-pinterest-p"></i>
                    </a>
                </div>
            </div>

            <div class="col-sm-6 col-lg-3 p-b-50">
                <h4 class="stext-301 cl0 p-b-30">
                    Newsletter
                </h4>

                <form>
                    <div class="wrap-input1 w-full p-b-4">
                        <input class="input1 bg-none plh1 stext-107 cl7" type="text" name="email" placeholder="email@example.com">
                        <div class="focus-input1 trans-04"></div>
                    </div>

                    <div class="p-t-18">
                        <button class="flex-c-m stext-101 cl0 size-103 bg1 bor1 hov-btn2 p-lr-15 trans-04">
                            Subscribe
                        </button>
                    </div>
                </form>
            </div>
        </div>

        <div class="p-t-40">
            <div class="flex-c-m flex-w p-b-18">
                <a href="#" class="m-all-1">
                    <img src="{{ asset('images/icons/icon-pay-01.png') }}" alt="ICON-PAY">
                </a>

                {{-- <a href="#" class="m-all-1">
                    <img src="{{ asset('images/icons/icon-pay-02.png') }}" alt="ICON-PAY">
                </a>

                <a href="#" class="m-all-1">
                    <img src="{{ asset('images/icons/icon-pay-03.png') }}" alt="ICON-PAY">
                </a>

                <a href="#" class="m-all-1">
                    <img src="{{ asset('images/icons/icon-pay-04.png') }}" alt="ICON-PAY">
                </a>

                <a href="#" class="m-all-1">
                    <img src="{{ asset('images/icons/icon-pay-05.png') }}" alt="ICON-PAY">
                </a> --}}
            </div>

            <p class="stext-107 cl6 txt-center">
                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->

            </p>
        </div>
    </div>
</footer>


<!-- Back to top -->
<div class="btn-back-to-top" id="myBtn">
    <span class="symbol-btn-back-to-top">
        <i class="zmdi zmdi-chevron-up"></i>
    </span>
</div>

<!--===============================================================================================-->	
<script src="{{ asset('/vendor/jquery/jquery-3.2.1.min.js') }}"></script>

<!--===============================================================================================-->
	<script src="{{ asset('vendor/animsition/js/animsition.min.js') }}"></script>
<!--===============================================================================================-->
	<script src="{{ asset('vendor/bootstrap/js/popper.js') }}"></script>
	<script src="{{ asset('vendor/bootstrap/js/bootstrap.min.js') }}"></script>
<!--===============================================================================================-->
	<script src="{{ asset('vendor/select2/select2.min.js') }}"></script>
	<script>
		$(".js-select2").each(function(){
			$(this).select2({
				minimumResultsForSearch: 20,
				dropdownParent: $(this).next('.dropDownSelect2')
			});
		})
	</script>
<!--===============================================================================================-->
	<script src="{{ asset('vendor/daterangepicker/moment.min.js') }}"></script>
	<script src="{{ asset('vendor/daterangepicker/daterangepicker.js') }}"></script>
<!--===============================================================================================-->
	<script src="{{ asset('vendor/slick/slick.min.js') }}"></script>
	<script src="{{ asset('js/slick-custom.js') }}"></script>
<!--===============================================================================================-->
	<script src="{{ asset('vendor/parallax100/parallax100.js') }}"></script>
	<script>
        $('.parallax100').parallax100();
	</script>
<!--===============================================================================================-->
	<script src="{{ asset('vendor/MagnificPopup/jquery.magnific-popup.min.js') }}"></script>
	<script>
		$('.gallery-lb').each(function() { // the containers for all your galleries
			$(this).magnificPopup({
		        delegate: 'a', // the selector for gallery item
		        type: 'image',
		        gallery: {
		        	enabled:true
		        },
		        mainClass: 'mfp-fade'
		    });
		});
	</script>
<!--===============================================================================================-->
	<script src="{{ asset('vendor/isotope/isotope.pkgd.min.js') }}"></script>
<!--===============================================================================================-->
	<script src="{{ asset('vendor/sweetalert/sweetalert.min.js') }}"></script>
	<script>
		// $('.js-addwish-b2').on('click', function(e){
		// 	e.preventDefault();
		// });

		// $('.js-addwish-b2').each(function(){
		// 	var nameProduct = $(this).parent().parent().find('.js-name-b2').html();
		// 	$(this).on('click', function(){
		// 		swal(nameProduct, "добавлен в список желаний!", "success");

		// 		$(this).addClass('js-addedwish-b2');
		// 		$(this).off('click');
		// 	});
		// });

		$('.js-addwish-detail').each(function(){
			var nameProduct = $(this).parent().parent().parent().find('.js-name-detail').html();

			$(this).on('click', function(){
				swal(nameProduct, "добавлен в список желаний!", "success");

				$(this).addClass('js-addedwish-detail');
				$(this).off('click');
			});
		});

		/*---------------------------------------------*/
        function success_to_cart(nameProduct) {
            $('.js-addcart-detail').each(function(){
                var nameProduct = $(this).parent().parent().parent().parent().find('.js-name-detail').html();
                $(this).on('click', function(){
                    swal(nameProduct, "добавлен в корзину !", "success");
                });
            });
        }

	
	</script>
<!--===============================================================================================-->
	<script src="{{ asset('vendor/perfect-scrollbar/perfect-scrollbar.min.js') }}"></script>
	<script>
		$('.js-pscroll').each(function(){
			$(this).css('position','relative');
			$(this).css('overflow','hidden');
			var ps = new PerfectScrollbar(this, {
				wheelSpeed: 1,
				scrollingThreshold: 1000,
				wheelPropagation: false,
			});

			$(window).on('resize', function(){
				ps.update();
			})
		});
	</script>
<!--===============================================================================================-->
	<script src="{{ asset('js/main.js') }}"></script>

@yield('js')

<script>
			
		/*==================================================================
		[ +/- num product ]*/
		$('.btn-product-detail-down').on('click', function(){
			var numProduct = Number($(this).next().val());
			if(numProduct > 0) $(this).next().val(numProduct - 1);
		});

		$('.btn-product-detail-up').on('click', function(){
			var numProduct = Number($(this).prev().val());
			$(this).prev().val(numProduct + 1);
		});


        $(document).ready(function () {

            $('#contactform').on('submit', function (e) {
                e.preventDefault();
                 
                $.ajax({
                    type: 'POST',
                    url: '/contact-form',
                    data: $('#contactform').serialize(),
                    success: function (data) {
                        console.log(data);
                        if (data) {
                            $('#senderror').hide();
                            $('#sendmessage').show();
                        } else {
                            $('#senderror').show();
                            $('#sendmessage').hide();
                        }
                    },
                    error: function (error) {
                        $('#senderror').show();
                        $('#sendmessage').hide();
                        console.log(error);
                    }
                });
                
            });
        });
        
        
        </script>