<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

@include('head')


<body class="animsition">	
	<!-- Header -->
	<header class="{{ Route::currentRouteName() !== 'index' ? 'header-v4' : ''}}">
        @include('header')
	</header>

    @include('components.cart_modal')
    <!-- Title page -->
    <section class="bg-img1 txt-center p-lr-15 p-tb-92" style="background-image: url('{{ asset('images/bg-02.jpg') }}');">
        <h2 class="ltext-105 cl0 txt-center">
            
        </h2>
    </section>
    <section class="bg0 p-t-62 p-b-60">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-lg-9 p-b-80">

                    @yield('content')

                </div>
                <div class="col-md-4 col-lg-3 p-b-80">

                    @include('components.post_sidebar')

                </div>
            </div>
        </div>
    </section>	

	@include('components.modal_products')
	
	@include('footer')

</body>
</html>