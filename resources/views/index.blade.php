<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

@include('head')

<body class="animsition">	
	<!-- Header -->
	<header class="{{ Route::currentRouteName() !== 'index' ? 'header-v4' : ''}}">
        @include('header')
	</header>

	@include('components.cart_modal')
    
	@yield('content')

	@include('components.modal_products')
	
	@include('footer')

</body>
</html>