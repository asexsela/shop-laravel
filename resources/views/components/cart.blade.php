

<!-- Shoping Cart -->
<div class="bg0 p-t-75 p-b-85">
		<div class="container">
			<div class="row">
				<div class="col-lg-10 col-xl-12 m-lr-auto m-b-50">
					<div class="m-l-25 m-r--38 m-lr-0-xl">
						@if (session()->has('success_message'))

							<div class="alert alert-success">
								{{ session()->get('success_message') }}
							</div>
							
						@endif

						@if (count($errors) > 0)
							
							<div class="alert alert-danger">
								<ul>
									@foreach ($errors->all() as $error)
										<li>{{ $error }}</li>
									@endforeach
								</ul>
							</div>

						@endif

						@if (Cart::count() > 0)
							
							<div class="wrap-table-shopping-cart">
								<table class="table-shopping-cart">
									<tr class="table_head">
										<th class="column-1">Товар</th>
										<th class="column-2"></th>
										<th class="column-3">Количество</th>
										{{-- <th class="column-4">Цена</th> --}}

										<th class="column-5">Цена</th>
									</tr>
									@foreach (Cart::content() as $item)
										
										<tr class="table_row">
											<td class="column-1">
												<div class="how-itemcart1">											
													<form class="item_cart_form" action="{{ route('cart.destroy', $item->rowId) }}" method="POST" >
														{{ csrf_field() }}
														{{ method_field('DELETE') }}
				
														<img src="{{ asset($item->model['img']) }}" alt="IMG">
														<button type="submit" class="del_item"></button>									
													</form>
												</div>
											</td>
											<td class="column-2"><a href="#">
												{{ $item->model->name }}
											</a></td>
											<td class="column-3">
												<div class="wrap-num-product flex-w m-l-auto m-r-0">
													<div class="btn-num-product-down cl8 hov-btn3 trans-04 flex-c-m">
														<i class="fs-16 zmdi zmdi-minus"></i>
													</div>

													<input data-id="{{ $item->rowId }}" class="mtext-104 cl3 txt-center num-product" type="number" name="num-product1" value="{{ $item->qty }}">

													<div class="btn-num-product-up cl8 hov-btn3 trans-04 flex-c-m">
														<i class="fs-16 zmdi zmdi-plus"></i>
													</div>
												</div>
											</td>
											<td class="column-5">{{ presentPrice($item->subtotal)  }} руб.</td>

											{{-- <td class="column-5">$ 36.00</td> --}}
										</tr>

									@endforeach

								</table>
							</div>
							@if (!session()->has('coupon'))
							<div class="flex-w flex-sb-m bor15 p-t-18 p-b-15 p-lr-40 p-lr-15-sm">
								<div class="flex-w flex-m m-r-20 m-tb-5">
									<form action="{{ route('coupon.store') }}" method="post" style="display: flex">
										{{ csrf_field() }}
										<input class="stext-104 cl2 plh4 size-117 bor13 p-lr-20 m-r-10 m-tb-5" type="text" name="coupon" placeholder="Код" id="coupon">
										<button class="flex-c-m stext-101 cl2 size-118 bg8 bor13 hov-btn3 p-lr-15 trans-04 pointer m-tb-5" type="submit" class="">Применить купон</button>
									</form>
										
								</div>
							</div>
							@endif

						@else
						
								<h3 class="ltext-103 cl5">Корзина пуста</h3>

						@endif

						@if (Cart::count() > 0)

						<div class="flex-w flex-sb-m bor15 p-t-18 p-b-15 p-lr-40 p-lr-15-sm">
							@if (session()->has('coupon'))
								<div class="flex-w flex-t bor12 p-b-13">
									<div>
										<span class="stext-110 cl2">
											Купон ({{ session()->get('coupon')['name'] }}):  
										</span>
									</div>
									&nbsp;&nbsp;
									<div>
										<span class="mtext-110 cl2">
											-{{ presentPrice($discount) }} руб.
											<form action="{{ route('coupon.destroy') }}" method="post" style="display: inline;">
												{{ csrf_field() }}
												{{ method_field('delete') }}
												<button type="submit" style="font-size: 14px;">Удалить</button>
											</form>
										</span>
									</div>
								</div>
							@endif
							<div class="flex-w flex-t bor12 p-b-13">
								<div>
									<span class="stext-110 cl2">
										Сумма:  
									</span>
								</div>
								&nbsp;&nbsp;
								<div>
									<span class="mtext-110 cl2">
										{{ presentPrice($newSubtotal) }} руб.
									</span>
								</div>
							</div>

							<div class="flex-w flex-t bor12 p-b-13">
								<div>
									<span class="stext-110 cl2">
										Доставка (13%):  
									</span>
								</div>
								&nbsp;&nbsp;
								<div>
									<span class="mtext-110 cl2">
											{{ presentPrice($newTax) }} руб.
									</span>
								</div>
							</div>
							<div class="flex-w flex-t bor12 p-b-13">
								<div>
									<span class="stext-110 cl2">
										Итого:  
									</span>
								</div>
								&nbsp;&nbsp;
								<div>
									<span class="mtext-110 cl2">
										{{ PresentPrice($newTotal) }} руб.
									</span>
								</div>
							</div>
						</div>
					</div>
					
					<form action="{{ route('cart.empty') }}">
						{{ csrf_field() }}
						<div class="flex-r-m ">
							<button class="stext-101 cl2 size-119 bg8 bor13 hov-btn3 p-lr-15 trans-04 pointer m-tb-10" type="submit">Очистить корзину</button>
						</div>
					</form>
					
					<div class="flex-r-m ">
						<a href="{{ route('checkout.index') }}" class="flex-c-m stext-101 cl2 size-119 bg8 bor13 hov-btn3 p-lr-15 trans-04 pointer m-tb-10">Оплата</a>
					</div>
						
					@else
						<div class="flex-r-m ">
							<a href="{{ route('shop') }}" class="flex-c-m stext-101 cl2 size-119 bg8 bor13 hov-btn3 p-lr-15 trans-04 pointer m-tb-10">Каталог</a>
						</div>
					@endif

				</div>

				{{-- <div class="col-sm-10 col-lg-7 col-xl-5 m-lr-auto m-b-50">
					<div class="bor10 p-lr-40 p-t-30 p-b-40 m-l-63 m-r-40 m-lr-0-xl p-lr-15-sm">
						<h4 class="mtext-109 cl2 p-b-30">
							Результат
						</h4>

						<div class="flex-w flex-t bor12 p-b-13">
							<div class="size-208">
								<span class="stext-110 cl2">
									Сумма:
								</span>
							</div>

							<div class="size-209">
								<span class="mtext-110 cl2">
									{{ Cart::subtotal() }} руб.
								</span>
							</div>
						</div>

						<div class="flex-w flex-t bor12 p-t-15 p-b-30">
							<div class="size-208 w-full-ssm">
								<span class="stext-110 cl2">
									Доставка:
								</span>
							</div>

							<div class="size-209 p-r-18 p-r-0-sm w-full-ssm">
								<p class="stext-111 cl6 p-t-2">
										Нет доступных способов доставки. Пожалуйста, проверьте ваш адрес или свяжитесь с нами, если вам нужна помощь.
								</p>
								
								<div class="p-t-15">
									<span class="stext-112 cl8">
										Выберете доставку
									</span>

									<div class="rs1-select2 rs2-select2 bor8 bg0 m-b-12 m-t-9">
										<select class="js-select2" name="time">
											<option>Select a country...</option>
											<option>USA</option>
											<option>UK</option>
										</select>
										<div class="dropDownSelect2"></div>
									</div>

									<div class="bor8 bg0 m-b-12">
										<input class="stext-111 cl8 plh3 size-111 p-lr-15" type="text" name="state" placeholder="State /  country">
									</div>

									<div class="bor8 bg0 m-b-22">
										<input class="stext-111 cl8 plh3 size-111 p-lr-15" type="text" name="postcode" placeholder="Postcode / Zip">
									</div>
									
									<div class="flex-w">
										<div class="flex-c-m stext-101 cl2 size-115 bg8 bor13 hov-btn3 p-lr-15 trans-04 pointer">
											Обновить
										</div>
									</div>
										
								</div>
							</div>
						</div>

						<div class="flex-w flex-t p-t-27 p-b-33">
							<div class="size-208">
								<span class="mtext-101 cl2">
									Доставка(13%):
								</span>
							</div>

							<div class="size-209 p-t-1">
								<span class="mtext-110 cl2">
									{{ Cart::tax() }}
								</span>
							</div>
							<div class="size-208">
									<span class="mtext-101 cl2">
										Итого:
									</span>
								</div>
	
								<div class="size-209 p-t-1">
									<span class="mtext-110 cl2">
										{{ Cart::total() }}
									</span>
								</div>
						</div>

						<button class="flex-c-m stext-101 cl0 size-116 bg3 bor14 hov-btn3 p-lr-15 trans-04 pointer">
							Оплата
						</button>
					</div>
				</div> --}}
			</div>
		</div>
	</div>
