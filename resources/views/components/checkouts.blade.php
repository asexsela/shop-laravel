
<style>

.checkout_product_box {
    display: flex;
    flex-wrap: wrap;
    /* justify-content: space-between; */
    width: 100%;
}
.checkout_product_box > .column2 {
    display: flex;
    flex-direction: column;
    width: 50%;
}
.checkout_product_box .column1 {
    width: 30%;
}

.checkout_product_box > div {
    margin: 5px;
}
.checkout_product_box img {
    width: 100%;
}
.checkout_right_price span {
    font-size: 16px;
}
.checkout_right_price {
    width: 60%;
}
.checkout_left_price {
    width: 40%;
    text-align: right;
}
.checkout_left_price span {
    font-size: 15px;
}
</style>
<!-- Shoping Cart -->
<div class="bg0 p-t-75 p-b-85">
		<div class="container">
                @if (session()->has('success_message'))

                <div class="alert alert-success">
                    {{ session()->get('success_message') }}
                </div>
                
            @endif

            @if (count($errors) > 0)
                
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>

            @endif
			<div class="row">
				<div class="col-lg-10 col-xl-7 m-lr-auto m-b-50">
                    <form action="{{ route('checkout.store') }}" method="POST" id="payment-form">
                        {{ csrf_field() }}

                        <div class="m-l-25 m-r--38 m-lr-0-xl">
                            <h4 class="mtext-105 cl2 js-name-detail p-b-14">
                                    Адрес:
                            </h4>
                        
                            <div class="form-group">
                                <label for="inputAddress">Email</label>
                                <input type="email" class="form-control" id="email_checkout" name="email_checkout" placeholder="admin@admin.com" value="{{ auth()->user()->email }}" readonly>
                            </div>
                            <div class="form-group">
                                <label for="inputAddress2">ФИО </label>
                                <input type="text" class="form-control" id="full_name_checkout" name="full_name_checkout" placeholder="Иванов Иван Иванович" value="{{ old('full_name_checkout') }}" required>
                            </div>
                            <div class="form-group">
                                <label for="inputAddress2">Адрес</label>
                                <input type="text" class="form-control" id="addr_one_checkout" name="addr_one_checkout" placeholder="ул. Мира 12, кв. 55" value="{{ old('addr_one_checkout') }}" required>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="inputEmail4">Город</label>
                                    <input type="text" class="form-control" id="city_checkout" name="city_checkout" placeholder="Москва" value="{{ old('city_checkout') }}" required>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="inputPassword4">Регион</label>
                                    <input type="text" class="form-control" id="reg_checkout" name="reg_checkout" placeholder="Кировский район" value="{{ old('reg_checkout') }}" required>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="inputEmail4">Индекс</label>
                                    <input type="text" class="form-control" id="postcode_checkout" name="postcode_checkout" placeholder="550160" value="{{ old('postcode_checkout') }}" required>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="inputPassword4">Телефон</label>
                                    <input type="phone" class="form-control" id="phone_checkout" name="phone_checkout" placeholder="+79999999999" value="{{ old('phone_checkout') }}" required>
                                </div>
                            </div>
                                                

                        </div>
                        <div class="m-l-25 m-r--38 m-lr-0-xl">
                            <h4 class="mtext-105 cl2 js-name-detail p-b-14">
                                    Данные оплаты:
                            </h4>
                            <div class="form-group">
                                    <label for="inputAddress">Имя с карты</label>
                                    <input type="text" class="form-control" id="name_cart_checkout" name="name_cart_checkout" placeholder="Иванов Иван" value="{{ old('name_cart_checkout') }}" required>
                                </div>
                                {{-- <div class="form-group">
                                    <label for="inputAddress2">Адрес </label>
                                    <input type="text" class="form-control" id="inputAddress2" placeholder="">
                                </div> --}}
                                {{-- <div class="form-group">
                                    <label for="inputAddress2">Номер карты</label>
                                    <input type="text" class="form-control" id="inputAddress2" placeholder="9999 9999 9999 9999">
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="inputEmail4">Дата с карты</label>
                                        <input type="text" class="form-control" id="inputEmail4" placeholder="12.12.12">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="inputPassword4">CVC</label>
                                        <input type="text" class="form-control" id="inputPassword4" placeholder="999">
                                    </div>
                                </div> --}}
                                <div class="form-group">
                                    <label for="card-element">
                                        Credit or debit card
                                    </label>
                                    <div id="card-element">
                                        <!-- A Stripe Element will be inserted here. -->
                                    </div>
                                
                                    <!-- Used to display form errors. -->
                                    <div id="card-errors" role="alert"></div>
                                </div>
                                <div class="flex-r-m ">
                                    <button id="complete-order" class="stext-101 cl6 size-119 bg3 bor14 hov-btn3 p-lr-15 trans-04 pointer" type="submit">Оплата</button>
                                </div>		
                        </div>
                    </form>
				</div>

				<div class="col-sm-10 col-lg-7 col-xl-5 m-lr-auto m-b-50">
					<div class="bor10 p-lr-30 p-t-30 p-b-40 m-l-63 m-r-40 m-lr-0-xl p-lr-15-sm">
						<h4 class="mtext-109 cl2 bor12">
							Товары
						</h4>

						<div class="flex-w flex-t bor12 p-b-13 checkout-content js-pscroll ps ps--active-y">
                            @foreach (Cart::content() as $item)
                                <div class="checkout_product_box">
                                    <div class="column1">
                                        <img src="{{ $item->model->img }}" alt="">
                                    </div>
                                    <div class="column2">
                                        <div class="check_out_product_name">{{ $item->model->name }}</div>
                                        <div class="check_out_product_price">{{ $item->model->presentPrice() }} .руб</div>
                                    </div>
                                    <div class="column3">
                                        <div class="check_out_product_quantity">{{ $item->qty }}</div>
                                    </div>
                                </div>
                            @endforeach
                            
                            
						</div>

						<div class="flex-w flex-t p-t-27 bor12 p-b-33">
							<div class="checkout_right_price">
								<span class="mtext-101 cl2">
									Сумма:
								</span>
							</div>

							<div class="checkout_left_price">
								<span class="mtext-110 cl2">
									{{ presentPrice($newSubtotal) }} .руб
								</span>
                            </div>
                            @if (session()->has('coupon'))

                            <div class="checkout_right_price">
                                <span class="mtext-101 cl2">
                                    Купон ({{ session()->get('coupon')['name'] }}):  
                                </span>
                            </div>

                            <div class="checkout_left_price">
                                <span class="mtext-110 cl2">
                                        -{{ presentPrice($discount) }} руб.
                                </span>
                            </div>
                            @endif
                            <div class="checkout_right_price">
                                <span class="mtext-101 cl2">
                                    Доставка(13%):
                                </span>
                            </div>

                            <div class="checkout_left_price">
                                <span class="mtext-110 cl2">
                                    {{ presentPrice($newTax) }} .руб
                                </span>
                            </div>
							<div class="checkout_right_price">
                                <span class="mtext-101 cl2">
                                   <b>Итого:</b> 
                                </span>
                            </div>

                            <div class="checkout_left_price">
                                <span class="mtext-110 cl2">
                                    <b>{{ presentPrice($newTotal) }} .руб</b>
                                </span>
                            </div>

                        </div>
                        
					</div>
				</div>
			</div>
		</div>
	</div>
