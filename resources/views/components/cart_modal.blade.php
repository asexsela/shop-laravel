	<!-- Cart -->
	<div class="wrap-header-cart js-panel-cart">
		<div class="s-full js-hide-cart"></div>

		<div class="header-cart flex-col-l p-l-65 p-r-25">
			<div class="header-cart-title flex-w flex-sb-m p-b-8">
				<span class="mtext-103 cl2">
					Корзина
				</span>

				<div class="fs-35 lh-10 cl2 p-lr-5 pointer hov-cl1 trans-04 js-hide-cart">
					<i class="zmdi zmdi-close"></i>
				</div>
			</div>
			
			<div class="header-cart-content flex-w js-pscroll">
				<ul class="header-cart-wrapitem w-full">
					@foreach (Cart::content() as $item)
						<li class="header-cart-item flex-w flex-t m-b-12">
							<div class="header-cart-item-img">
								<form class="item_cart_form" action="{{ route('cart.destroy', $item->rowId) }}" method="POST" >
									{{ csrf_field() }}
									{{ method_field('DELETE') }}

									<img src="{{ asset($item->model['img']) }}" alt="IMG">
									<button type="submit" class="del_item"></button>									
								</form>
							</div>

							<div class="header-cart-item-txt p-t-8">
								<a href="#" class="header-cart-item-name m-b-18 hov-cl1 trans-04">
									{{ $item->model->name }} ({{ Cart::count() }})
								</a>

								<span class="header-cart-item-info">
									{{ $item->model->presentPrice() }} руб.
								</span>
							</div>
						</li>
					@endforeach
				</ul>
				
				<div class="w-full">
					<div class="header-cart-total w-full p-tb-40">
						Итого: {{ Cart::subtotal() }} руб.
					</div>

					<div class="header-cart-buttons flex-w w-full">
						<a href="{{ route('cart.index') }}" class="flex-c-m stext-101 cl0 size-107 bg3 bor2 hov-btn3 p-lr-15 trans-04 m-r-8 m-b-10">
							Корзина
						</a>

						<a href="{{ route('checkout.index') }}" class="flex-c-m stext-101 cl0 size-107 bg3 bor2 hov-btn3 p-lr-15 trans-04 m-b-10">
							Оплата
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>