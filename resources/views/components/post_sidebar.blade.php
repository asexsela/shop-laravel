<div class="side-menu">
    <div class="bor17 of-hidden pos-relative">
        <input class="stext-103 cl2 plh4 size-116 p-l-28 p-r-55" type="text" name="search" placeholder="Search">

        <button class="flex-c-m size-122 ab-t-r fs-18 cl4 hov-cl1 trans-04">
            <i class="zmdi zmdi-search"></i>
        </button>
    </div>


    <div class="p-t-55">
        <h4 class="mtext-112 cl2 p-b-33">
            Категории
        </h4>

        <ul>

            @foreach ($postCategories as $postCat)
                <li class="bor18">
                    <a href="{{ route('blog.category', $postCat->slug) }}" class="{{ Request::path() == 'blog/category/' . $postCat->slug ? 'how-active1' : '' }} dis-block stext-115 cl6 hov-cl1 trans-04 p-tb-8 p-lr-4">
                        {{ $postCat->name }}
                    </a>
                </li>
            @endforeach
        </ul>
    </div>

    <div class="p-t-65">
        <h4 class="mtext-112 cl2 p-b-33">
            Возможно вам понравится
        </h4>

        <ul>
            @foreach ($featuredProducts as $featured)
                <li class="flex-w flex-t p-b-30">
                    <a href="{{ route('product', $featured->slug) }}" class="wrao-pic-w size-214 hov-ovelay1 m-r-20">
                        <img style="width:100%" src="{{ asset($featured->img) }}" alt="{{ $featured->name }}">
                    </a>

                    <div class="size-215 flex-col-t p-t-8">
                        <a href="{{ route('product', $featured->slug) }}" class="stext-116 cl8 hov-cl1 trans-04">
                            {{ $featured->name }}
                        </a>

                        <span class="stext-116 cl6 p-t-20">
                            {{ presentPrice($featured->price) }} .руб
                        </span>
                    </div>
                </li>
            @endforeach
        </ul>
    </div>

    <div class="p-t-55">
        <h4 class="mtext-112 cl2 p-b-20">
            Archive
        </h4>

        <ul>
            @foreach ($arhives as $item)
                
                <li class="p-b-7">
                    <a href="#" class="flex-w flex-sb-m stext-115 cl6 hov-cl1 trans-04 p-tb-2">
                        <span>
                            {{ Date::parse($item->created_at)->format('F Y') }}
                        </span> 
                        {{-- <span>
                            (9)
                        </span> --}}
                    </a>
                </li>
            @endforeach

        </ul>
    </div>

    <div class="p-t-50">
        <h4 class="mtext-112 cl2 p-b-27">
            Tags
        </h4>

        <div class="flex-w m-r--5">
            <a href="#" class="flex-c-m stext-107 cl6 size-301 bor7 p-lr-15 hov-tag1 trans-04 m-r-5 m-b-5">
                Fashion
            </a>

            <a href="#" class="flex-c-m stext-107 cl6 size-301 bor7 p-lr-15 hov-tag1 trans-04 m-r-5 m-b-5">
                Lifestyle
            </a>

            <a href="#" class="flex-c-m stext-107 cl6 size-301 bor7 p-lr-15 hov-tag1 trans-04 m-r-5 m-b-5">
                Denim
            </a>

            <a href="#" class="flex-c-m stext-107 cl6 size-301 bor7 p-lr-15 hov-tag1 trans-04 m-r-5 m-b-5">
                Streetstyle
            </a>

            <a href="#" class="flex-c-m stext-107 cl6 size-301 bor7 p-lr-15 hov-tag1 trans-04 m-r-5 m-b-5">
                Crafts
            </a>
        </div>
    </div>
</div>