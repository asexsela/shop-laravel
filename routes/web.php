<?php

use DemeterChain\C;


Route::get('/', 'IndexController@index')->name('index');
Route::post('/product_details/{slug}', 'CatalogController@product_details')->name('product_details');
Route::post('/search/{s}', 'CatalogController@search')->name('search');
// shop
Route::get('/shop', 'CatalogController@index')->name('shop');
Route::get('/shop/{slug}', 'CatalogController@category')->name('shop.category');
Route::get('/shop/product/{product}', 'CatalogController@indexProduct')->name('product');
// cart
Route::get('/cart', 'CartController@index')->name('cart.index');
Route::post('/cart', 'CartController@store')->name('cart.store');
Route::patch('/cart/{product}', 'CartController@update')->name('cart.update');
Route::delete('/cart/{product}', 'CartController@destroy')->name('cart.destroy');
Route::post('/addtocart/{id}/{amount}/{size}/{color}', 'CartController@ajaxStore')->name('ajaxStore');

Route::post('/letter/{product}', 'LetterController@saveLetter')->name('letter.save');

Route::get('/empty', 'CartController@empty')->name('cart.empty');
Route::get('/checkout', 'CheckoutController@index')->name('checkout.index')->middleware('auth');
Route::post('/checkout', 'CheckoutController@store')->name('checkout.store');
Route::get('/confirm', 'ConfirmationController@index')->name('confirm.index');

Route::post('/coupon', 'CouponsController@store')->name('coupon.store');
Route::delete('/coupon', 'CouponsController@destroy')->name('coupon.destroy');
// blog
Route::get('/blog', 'BlogController@index')->name('blog');
Route::get('/blog/{alias}', 'BlogController@indexDetails')->name('blog-details');
Route::get('/blog/category/{alias}', 'BlogController@getCategory')->name('blog.category');

Route::post('/contact-form', 'SendController@sendContacts');

Route::get('/track-order', function() {
    return view('pages.track-order');
})->name('track_order');
Route::get('/order-return', function() {
    return view('pages.order-return');
})->name('order_return');
Route::get('/shiping', function() {
    return view('pages.shiping');
})->name('shiping');

Route::get('/about', 'AboutController@index')->name('about');
Route::match(['get','post'], '/contacts', 'ContactsController@index')->name('contacts');
Route::match(['get','post'], '/faq', 'FaqController@index')->name('faq');
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
Route::group(['prefix' => '/admin'], function () {
    Voyager::routes();
});
